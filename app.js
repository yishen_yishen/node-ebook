const express = require('express')
const mysql = require('mysql')
const app = express()
const constant = require('./const')
const cors = require('cors')
app.use(cors())
// 连接数据库数据库的方法，每次请求之前先调用此方法，请求完之后，在.end()
function connection () {
  return mysql.createConnection({
    host: '118.89.55.75',
    user: 'api_mdashen_com',
    password: 'kSyPPMC3rJ2cNsX2',
    database: 'api_mdashen_com'
  })
}
// 链接数据库，判断哪是否连接成功
const connnect = connection(function (err) {
  if (err) {
    console.error('数据库连接失败' + err)
    return
  }
  console.info('数据库连接成功：线程ID: ' + connection.threadId);
})
// node服务器监听端口
const server = app.listen(3000, () => {
  const host = server.address().address
  const port = server.address().port
  console.info('服务器启动成功 http://%s:$s', host, port);
})

function createGuessYouLike (data) {
  const n = parseInt(randomArray(1, 3)) + 1
  data['type'] = n
  // 仅模拟数据，相关功能并未实现(具体实现比较复杂)
  switch (n) {
    case 1:
      data['result'] = data.id % 2 === 0 ? '《Executig Magic》' : '《Elements Of Robotics》'
      break;
    case 2:
      data['result'] = data.id % 2 === 0 ? '《Living with Disfigurement》' : '《Programming Languages》'
      break;
    case 3:
      data['result'] = '《Programming Languages》'
      data['percent'] = data.id % 2 === 0 ? '92%' : '97%'
      break;

    default:
      break;
  }
  return data
}
function createRecommend (data) {
  data['readers'] = Math.floor(data.id / 2 * randomArray(1, 100))
  return data
}
function createCategoryIds (n) {
  const arr = []
  constant.category.forEach((item, index) => {
    arr.push(index + 1)
  })
  const result = []
  for (let i = 0; i < n; i++) {
    // 获取的随机数不能重复
    const ran = Math.floor(Math.random() * (arr.length - i))
    // 获取分类对应的序号
    result.push(arr[ran])
    // 将已经获取的随机数取代，用最后一位数
    arr[ran] = arr[arr.length - i - 1]
  }
  return result
}
function createCategoryData (data) {
  const categoryIds = createCategoryIds(6)
  const result = []
  categoryIds.forEach(categoryId => {
    const subList = data.filter(item => item.category === categoryId).slice(0, 4)
    subList.map(item => {
      return handleData(item)
    })
    result.push({
      category: categoryId,
      list: subList
    })
  })
  return result.filter(item => item.list.length === 4)
}
function randomArray (n, l) {  // 获取随机数(需要几个数,total)，返回有n个随机数组成的数组(随机数最大值为l)
  let rnd = []
  for (let i = 0; i < n; i++) {
    rnd.push(Math.floor(Math.random() * l))
  }
  return rnd
}
// 替换results中的图片路径(相对路径替换为绝对路径)
function createData (results, key) {
  return handleData(results[key])
}
function handleData (data) {
  if (!data.cover.startsWith('http://')) { // 数据库中的cover路径是相对路径
    data['cover'] = `${constant.resUrl}/img` + data.cover
  }
  data['selected'] = false
  data['private'] = false
  data['cache'] = false
  data['haveRead'] = false
  return data
}
app.get('/', (req, res) => {
  res.send(new Date().toDateString())
})
app.get('/book/home', (req, res) => {
  const conn = connection()
  conn.query('select * from book where cover !=\'\'', (err, results) => {  // !==''不为空，\是转义符
    if (err) {
      res.json({
        error_code: 1,
        err,
        msg: '获取数据失败'
      })
      return
    }
    const total = results.length
    const banner = `${constant.resUrl}/home_banner.jpg`
    const categories = [
      {
        category: 1,
        num: 56,
        img1: constant.resUrl + '/cover/cs/A978-3-319-62533-1_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/cs/A978-3-319-89366-2_CoverFigure.jpg'
      },
      {
        category: 2,
        num: 51,
        img1: constant.resUrl + '/cover/ss/A978-3-319-61291-1_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/ss/A978-3-319-69299-9_CoverFigure.jpg'
      },
      {
        category: 3,
        num: 32,
        img1: constant.resUrl + '/cover/eco/A978-3-319-69772-7_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/eco/A978-3-319-76222-7_CoverFigure.jpg'
      },
      {
        category: 4,
        num: 60,
        img1: constant.resUrl + '/cover/edu/A978-981-13-0194-0_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/edu/978-3-319-72170-5_CoverFigure.jpg'
      },
      {
        category: 5,
        num: 23,
        img1: constant.resUrl + '/cover/eng/A978-3-319-39889-1_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/eng/A978-3-319-00026-8_CoverFigure.jpg'
      },
      {
        category: 6,
        num: 42,
        img1: constant.resUrl + '/cover/env/A978-3-319-12039-3_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/env/A978-4-431-54340-4_CoverFigure.jpg'
      },
      {
        category: 7,
        num: 7,
        img1: constant.resUrl + '/cover/geo/A978-3-319-56091-5_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/geo/978-3-319-75593-9_CoverFigure.jpg'
      },
      {
        category: 8,
        num: 18,
        img1: constant.resUrl + '/cover/his/978-3-319-65244-3_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/his/978-3-319-92964-4_CoverFigure.jpg'
      },
      {
        category: 9,
        num: 13,
        img1: constant.resUrl + '/cover/law/2015_Book_ProtectingTheRightsOfPeopleWit.jpeg',
        img2: constant.resUrl + '/cover/law/2016_Book_ReconsideringConstitutionalFor.jpeg'
      },
      {
        category: 10,
        num: 24,
        img1: constant.resUrl + '/cover/ls/A978-3-319-27288-7_CoverFigure.jpg',
        img2: constant.resUrl + '/cover/ls/A978-1-4939-3743-1_CoverFigure.jpg'
      },
      {
        category: 11,
        num: 6,
        img1: constant.resUrl + '/cover/lit/2015_humanities.jpg',
        img2: constant.resUrl + '/cover/lit/A978-3-319-44388-1_CoverFigure_HTML.jpg'
      },
      {
        category: 12,
        num: 14,
        img1: constant.resUrl + '/cover/bio/2016_Book_ATimeForMetabolismAndHormones.jpeg',
        img2: constant.resUrl + '/cover/bio/2017_Book_SnowSportsTraumaAndSafety.jpeg'
      },
      {
        category: 13,
        num: 16,
        img1: constant.resUrl + '/cover/bm/2017_Book_FashionFigures.jpeg',
        img2: constant.resUrl + '/cover/bm/2018_Book_HeterogeneityHighPerformanceCo.jpeg'
      },
      {
        category: 14,
        num: 16,
        img1: constant.resUrl + '/cover/es/2017_Book_AdvancingCultureOfLivingWithLa.jpeg',
        img2: constant.resUrl + '/cover/es/2017_Book_ChinaSGasDevelopmentStrategies.jpeg'
      },
      {
        category: 15,
        num: 2,
        img1: constant.resUrl + '/cover/ms/2018_Book_ProceedingsOfTheScientific-Pra.jpeg',
        img2: constant.resUrl + '/cover/ms/2018_Book_ProceedingsOfTheScientific-Pra.jpeg'
      },
      {
        category: 16,
        num: 9,
        img1: constant.resUrl + '/cover/mat/2016_Book_AdvancesInDiscreteDifferential.jpeg',
        img2: constant.resUrl + '/cover/mat/2016_Book_ComputingCharacterizationsOfDr.jpeg'
      },
      {
        category: 17,
        num: 20,
        img1: constant.resUrl + '/cover/map/2013_Book_TheSouthTexasHealthStatusRevie.jpeg',
        img2: constant.resUrl + '/cover/map/2016_Book_SecondaryAnalysisOfElectronicH.jpeg'
      },
      {
        category: 18,
        num: 16,
        img1: constant.resUrl + '/cover/phi/2015_Book_TheOnlifeManifesto.jpeg',
        img2: constant.resUrl + '/cover/phi/2017_Book_Anti-VivisectionAndTheProfessi.jpeg'
      },
      {
        category: 19,
        num: 10,
        img1: constant.resUrl + '/cover/phy/2016_Book_OpticsInOurTime.jpeg',
        img2: constant.resUrl + '/cover/phy/2017_Book_InterferometryAndSynthesisInRa.jpeg'
      },
      {
        category: 20,
        num: 26,
        img1: constant.resUrl + '/cover/psa/2016_Book_EnvironmentalGovernanceInLatin.jpeg',
        img2: constant.resUrl + '/cover/psa/2017_Book_RisingPowersAndPeacebuilding.jpeg'
      },
      {
        category: 21,
        num: 3,
        img1: constant.resUrl + '/cover/psy/2015_Book_PromotingSocialDialogueInEurop.jpeg',
        img2: constant.resUrl + '/cover/psy/2015_Book_RethinkingInterdisciplinarityA.jpeg'
      },
      {
        category: 22,
        num: 1,
        img1: constant.resUrl + '/cover/sta/2013_Book_ShipAndOffshoreStructureDesign.jpeg',
        img2: constant.resUrl + '/cover/sta/2013_Book_ShipAndOffshoreStructureDesign.jpeg'
      }
    ]
    const categoryList = createCategoryData(results)
    const featured = []
    const guessYouLike = []
    const random = []
    const recommend = []
    randomArray(9, total).forEach(key => {
      guessYouLike.push(createGuessYouLike(createData(results, key)))
    })
    randomArray(3, total).forEach(key => {
      recommend.push(createRecommend(createData(results, key)))
    })
    randomArray(6, total).forEach(key => {
      featured.push(createData(results, key))
    })
    randomArray(1, total).forEach(key => {
      random.push(createData(results, key))
    })
    res.json({
      error_code: 0,
      guessYouLike, banner, categories, categoryList, featured, random, recommend
    })
    conn.end()
  })
})
app.get('/book/detail', (req, res) => {
  const conn = connection()
  const fileName = req.query.fileName
  const sql = `select * from book where fileName = '${fileName}'`
  conn.query(sql, (err, result) => {
    if (err) {
      res.json({
        error_code: 1,
        msg: '电子书详情获取失败',
        err
      })
    } else {
      if (result?.length === 0) {
        res.json({
          error_code: 1,
          msg: '电子书详情获取失败'
        })
      } else {
        const book = handleData(result[0])
        res.json({
          error_code: 0,
          msg: '电子书详情获取成功',
          data: book
        })
      }
    }
  })
  conn.end()
})
// 查询图书分类
app.get('/book/list', (req, res) => {
  // TODO 这里可以优化，返回list时，获取一个分类下的list返回的也是全部list
  const conn = connection()
  conn.query('select * from book where cover != \'\' ', (err, result) => {
    if (err) {
      res.json({
        error_code: 1,
        err,
        msg: '获取失败'
      })
    } else {
      result.map(item => handleData(item))
      const data = {}
      constant.category.forEach(categoryText => {
        data[categoryText] = result.filter(item => item.categoryText === categoryText)
      })
      res.json({
        error_code: 0,
        data,
        total: result.length,
        msg: '获取成功'
      })
    }
    conn.end()
  })
})
app.get('/book/flat-list', (req, res) => {
  const conn = connection()
  conn.query('select * from book where cover != \'\' ', (err, result) => {
    if (err) {
      res.json({
        error_code: 1,
        err,
        msg: '获取失败'
      })
    } else {
      result.map(item => handleData(item))
      res.json({
        error_code: 0,
        data: result,
        total: result.length,
        msg: '获取成功'
      })
    }
    conn.end()
  })
})
app.get('/book/shelf', (req, res) => {
  res.json({
    bookList: []
  })
})